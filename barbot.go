package main

import (
        "fmt"
        "net/http"
        "os"
        "time"
        "github.com/stianeikeland/go-rpio"
)

var (
    // Use mcu pin 10, corresponds to physical pin 19 on the pi
    Relay1 = rpio.Pin(24) //cranberry
    Relay2 = rpio.Pin(4)  //oj
    Relay3 = rpio.Pin(25)
    Relay4 = rpio.Pin(4) 
    Relay5 = rpio.Pin(17) //Whiskey
    Relay6 = rpio.Pin(22) //vodka
    Relay7 = rpio.Pin(23) //led top
    Relay8 = rpio.Pin(9)  //led bottom
)

func makeDrink(drinkInt string) {

    switch drinkInt {
        case "1":
            //screwdriver
            Relay2.Toggle()
            time.Sleep(50 * time.Second) //oj
            Relay2.Toggle()
            Relay6.Toggle()
            time.Sleep(15 * time.Second) //vodka
            Relay6.Toggle()
            fmt.Println("made screwdriver")
        case "2":
            //shot o vodka
            Relay6.Toggle()
            time.Sleep(30 * time.Second) //vodka
            Relay6.Toggle()
            fmt.Println("made vodka")
        case "3":
            Relay5.Toggle()
            time.Sleep(30 * time.Second) //bourbon
            Relay5.Toggle()
            fmt.Println("made bourbon")
        case "4":
            Relay1.Toggle()
            time.Sleep(50 * time.Second) //cran
            Relay1.Toggle()
            Relay6.Toggle()
            time.Sleep(15 * time.Second) //vodka
            Relay6.Toggle()
            fmt.Println("made CranVodka")
	case "5":
	    Relay2.Toggle()
            time.Sleep(50 * time.Second) //oj
            Relay2.Toggle() 
            Relay5.Toggle() 
            time.Sleep(15 * time.Second) //whiskey
            Relay5.Toggle()
            fmt.Println("made whiskey sour")
        case "light":
            lightsToggle(1)
    }
}

func handler(w http.ResponseWriter, r *http.Request) {    
    makeDrink(r.URL.Path[1:])
    lightsToggle(2)
    fmt.Fprintf(w, "Drink complete")
}

func initBoard() {
    if err := rpio.Open(); err != nil {
        fmt.Println(err)
        os.Exit(1)
    }
    defer rpio.Close()
}

func lightsToggle(count int) {
    for i := 0; i < count; i++ {
        Relay8.Toggle()     
        time.Sleep(time.Second / 4)
    } 
}

func main() {
    //init
    if err := rpio.Open(); err != nil {
        fmt.Println(err)
        os.Exit(1)
    }
    defer rpio.Close() 
    fmt.Println(1)
    Relay1.Output()
    Relay1.High()
    fmt.Println(2)
    Relay2.Output()
    Relay2.High()
    fmt.Println(3)
    Relay3.Output()
    Relay3.High()
    fmt.Println(4)
    Relay4.Output()
    Relay4.High()
    fmt.Println(5)
    Relay5.Output()
    Relay5.High()
    fmt.Println(6)
    Relay6.Output()
    Relay6.High()
    fmt.Println(7)
    Relay7.Output()
    Relay7.Low()
    fmt.Println(8)
    Relay8.Output()
    Relay8.Low()

    lightsToggle(2)
    //start httplistener
    http.HandleFunc("/", handler)
    http.ListenAndServe(":8080", nil)
}
